package edu.deru.cs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerSegmentationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerSegmentationApplication.class, args);
	}

}
